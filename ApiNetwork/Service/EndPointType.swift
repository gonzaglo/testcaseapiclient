//
//  EndPointType.swift
//  ApiNetwork
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

