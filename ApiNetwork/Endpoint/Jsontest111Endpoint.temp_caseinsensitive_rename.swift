//
//  Jsontest111Endpoint.swift
//  Network
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation

public enum JsonTest111 {
    case androids
}

extension JsonTest111: EndPointType {
    
    var baseURL: URL {
        guard let url = URL(string: "http://private-db05-jsontest111.apiary-mock.com/") else {
            fatalError("baseURL could not be configured.")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .androids
            return "androids"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .androids:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
