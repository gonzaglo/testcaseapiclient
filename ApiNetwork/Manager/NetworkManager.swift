//
//  NetworkManager.swift
//  ApiNetwork
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation

enum NetworkResponse:String {
    case success
    case authenticationError = "Token required."
    case notFound = "Not found"
    case badRequest = "Bad request"
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "Could not decode the response."
}

enum Result<String>{
    case success
    case failure(String)
}

class NetworkManager {
    static let shared = NetworkManager()
}

extension HTTPURLResponse {
    func handleResponse() -> Result<String>{
        switch self.statusCode {
        case 200...299: return .success
        case 400: return .failure(NetworkResponse.badRequest.rawValue)
        case 401...403: return .failure(NetworkResponse.authenticationError.rawValue)
        case 404: return .failure(NetworkResponse.notFound.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}
