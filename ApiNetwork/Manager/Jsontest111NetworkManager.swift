//
//  Jsontest111NetworkManager.swift
//  ApiNetwork
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation

public class Jsontest111NetworkManager {
    
    public static let shared = Jsontest111NetworkManager()
    
    fileprivate init(){}
    
    private let router = Router<JsonTest111>()
    
    public func getAndroids<T:Codable>(completion: @escaping (_ models: [T]? , _ error:String?)->() ) {
        
        router.request(.androids) { data, error in
            
            guard let responseData = data else {
                completion(nil, error)
                return
            }
            
            do {
                print(responseData)
                let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                print(jsonData)
                let models = try JSONDecoder().decode([T].self, from: responseData)
                completion(models,nil)
            }catch {
                print(error)
                completion(nil, NetworkResponse.unableToDecode.rawValue)
            }
        }
    }
}
