//
//  ListController.swift
//  TestCaseApiClient
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ListController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let disposeBag = DisposeBag()
    private let viewModel = ListViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        bind()
        refresh()
    }
    
    @IBAction func refreshTap(_ sender: Any) {
        refresh()
    }
    
    private func bind(){
        
        searchBar.rx.text.orEmpty
            .asObservable()
            .bind(to: viewModel.searchQuery)
            .disposed(by: disposeBag)
        
        viewModel.data.asObservable()
            .subscribe(onNext:{ _ in
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
        
    }
    
}

extension ListController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: TitleCell.self, for: indexPath)
        let item = viewModel.data.value[indexPath.row]
        cell.titleLabel.text = item.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.currentIndex = indexPath.row
        let vc = storyboard?.instantiateViewController(withIdentifier: String(describing: PreviewController.self)) as! PreviewController
        vc.viewModel = viewModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    private func configureTableView(){
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(cell: TitleCell.self)
        tableView.refreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(handleRefresh(_:)),
                                     for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor.red
            
            return refreshControl
        }()
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl){
        refresh()
    }
    
    private func refresh(){
        if viewModel.isRefreshing {return}
        viewModel.isRefreshing = true
        tableView.refreshControl?.beginRefreshing()
        viewModel.loadData {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.refreshControl?.endRefreshing()
                self.searchBar.text = nil
                self.viewModel.isRefreshing = false
            }
        }
    }
    
}
