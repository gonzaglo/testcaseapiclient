//
//  PreviewController.swift
//  TestCaseApiClient
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit
import Kingfisher
import RxCocoa
import RxSwift

class PreviewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel:ListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        collectionView.layoutIfNeeded()
        collectionView.scrollToItem(at: IndexPath(item: viewModel.currentIndex, section: 0), at: .left, animated: false)
    }
    
}

extension PreviewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.data.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PreviewCollectionCell.self, for: indexPath)
        let item = viewModel.data.value[indexPath.row]
        cell.imageView.kf.setImage(with: item.img)
        cell.titleLabel.text = item.title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    private func configCollectionView(){
        collectionView.registerCell(with: PreviewCollectionCell.self)
    }
    
}
