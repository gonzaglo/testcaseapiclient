//
//  ListViewModel.swift
//  TestCaseApiClient
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation
import ApiNetwork
import RxSwift



class ListViewModel {
    
    private var originData:Variable<[AndroidModel]> = Variable([])
    var data:Variable<[AndroidModel]> = Variable([])
    var isRefreshing = false
    var searchQuery:Variable<String?> = Variable(nil)
    var currentIndex = 0
    let disposeBag = DisposeBag()
    
    
    init() {
        bind()
    }
    
    private func bind() {
        searchQuery
            .asObservable()
            .subscribe(onNext:{ query in
                
                if self.originData.value.isEmpty {
                    self.data.value = []
                    return
                }
                
                guard let query = query else {
                    self.data.value = self.originData.value
                    return
                }
                
                if query.isEmpty {
                    self.data.value = self.originData.value
                    return
                }
                
                self.data.value = self.originData.value.filter({(androidModel: AndroidModel) -> Bool in
                    return androidModel.title?.range(of: query, options: .caseInsensitive) != nil
                })
            }).disposed(by: disposeBag)
    }
    
    func loadData(completion: @escaping ()->Void) {
        searchQuery.value = nil
        Jsontest111NetworkManager.shared.getAndroids { [weak self] (models: [AndroidModel]?, error) in
            guard let `self` = self else {return}
            
            
            self.originData.value = models ?? []
            self.data.value = self.originData.value
            
            completion()
        }
    }
}
