//
//  AndroidModel.swift
//  TestCaseApiClient
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation


class AndroidModel:Codable {
    var id:Int?
    var title:String?
    var img:URL?
}
