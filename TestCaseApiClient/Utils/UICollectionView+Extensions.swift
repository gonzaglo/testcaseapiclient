//
//  UICollectionView+Extensions.swift
//  TestCaseApiClient
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit

extension UICollectionView {
    public func registerCell<T:UICollectionViewCell>(with type:T.Type){
        let nibName = UINib(nibName: String(describing: T.self), bundle:nil)
        self.register(nibName, forCellWithReuseIdentifier: String(describing: T.self))
    }
    
    public func dequeueReusableCell<T: UICollectionViewCell>(with type: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as! T
    }
}
