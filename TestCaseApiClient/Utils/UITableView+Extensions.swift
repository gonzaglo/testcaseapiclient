//
//  UITableView+Extensions.swift
//  TestCaseApiClient
//
//  Created by Георгий Кузьминых on 20/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit

extension UITableView {
    
    public func register<T: UITableViewCell>(cell: T.Type, bundle: Bundle? = nil) {
        let nib = UINib(nibName: String(describing: T.self), bundle: bundle)
        register(nib, forCellReuseIdentifier: String(describing: T.self))
    }
    
    public func dequeueReusableCell<T: UITableViewCell>(with type: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as! T
    }
    
}
